<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seller = \App\Role::create(['name' => 'Seller']);
        $buyer= \App\Role::create(['name' => 'Buyer']);


        \App\User::find(1)->roles()->attach($seller->id);
       \App\User::find(2)->roles()->attach($buyer->id);
    }
}
