<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\Product::class, 5)->make();

        \App\Product::create([
            'name' => 'Samsung',
            'price' => 100,
            'user_id' => 1
        ]);
        \App\Product::create([
            'name' => 'Apple',
            'price' => 200,
            'user_id' => 1
        ]);
        \App\Product::create([
            'name' => 'Huwei',
            'price' => 600,
            'user_id' => 1
        ]);
        \App\Product::create([
            'name' => 'Sony',
            'price' => 900,
            'user_id' => 1
        ]);
        \App\Product::create([
            'name' => 'Acer',
            'price' => 700,
            'user_id' => 1
        ]);



    }
}
