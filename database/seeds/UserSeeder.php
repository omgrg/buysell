<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'suman',
            'email' => 'suman.gurung@hotmail.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);

        App\User::create([
            'name' => 'uday',
            'email' => 'uday@hotmail.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);
    }
}
