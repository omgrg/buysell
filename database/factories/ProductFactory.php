<?php

use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {
    return [
        'name'  => $faker->name,
        'price'  => rand(5,1000),
        'user_id' => rand(1,2)
    ];
});
