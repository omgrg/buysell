@extends('layouts.app')

@section('content')

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @foreach($products as $product)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$product->name}}
                        <br>
                            Custom Helper Product name reverse{{StringHelper::stringReverse($product->name)}}
                        </div>


                        <div class="panel-body">

                                <div class="alert alert-success">
                                  Price:-    {{$product->price}}<br>

                                    Update
                                    @can('view', $product)
                                    <form action="{{route('store.update',['id'=> $product->id])}}" method="POST">
                                        {{ method_field('PUT') }}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        Name: <input type="text" name="name" value="{{$product->name}}"><br>
                                        Price : <input type="text" name="price" value="{{$product->price}}"><br>
                                        <input type="submit" value="Submit">
                                    </form>
                                        @endcan

                                </div>

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
@endsection