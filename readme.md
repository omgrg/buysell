1. create database:-
databasename:- buyerseller
collation:- utf8mb4_unicode_ci

for mysql username and password please change in .env file.

2. open command prompt in root directory
3. composer install or composer update
4. type this command:- php artisan migrate
5. type this command:- php artisan db:seed
6. Seller Account:- suman.gurung@hotmail.com
    Seller Pass:- secret
    Buyer Account:- uday@hotmail.com
    buyer pass:- secret
    
 7. only seller account can edit product name and price. Unauthenticated user and buyer account cannot
 edit. 
